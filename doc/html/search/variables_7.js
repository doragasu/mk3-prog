var searchData=
[
  ['ines2_5fram_5f0b',['INES2_RAM_0B',['../ines_8h.html#afd92900471f943256a7164c00e0ff489',1,'ines.h']]],
  ['ines2_5fram_5f1024kb',['INES2_RAM_1024KB',['../ines_8h.html#ae28fc1fb47b27fdef4436cf3a38ecac4',1,'ines.h']]],
  ['ines2_5fram_5f128b',['INES2_RAM_128B',['../ines_8h.html#a37978cc2d6584fbbcd1fd62e7aa1c8a2',1,'ines.h']]],
  ['ines2_5fram_5f128kb',['INES2_RAM_128KB',['../ines_8h.html#a66d1cb8d7c2c211e1d2de9ed13a1807e',1,'ines.h']]],
  ['ines2_5fram_5f16kb',['INES2_RAM_16KB',['../ines_8h.html#a59df2e77ce3c4d1c9af27ea773a8a123',1,'ines.h']]],
  ['ines2_5fram_5f1kb',['INES2_RAM_1KB',['../ines_8h.html#a0e173de3c215de71f26255a700510a78',1,'ines.h']]],
  ['ines2_5fram_5f256b',['INES2_RAM_256B',['../ines_8h.html#a7f748e3ef2be5546e1ac0694afe5ce45',1,'ines.h']]],
  ['ines2_5fram_5f256kb',['INES2_RAM_256KB',['../ines_8h.html#a38f7fd6dba34fb157f691487f9567db5',1,'ines.h']]],
  ['ines2_5fram_5f2kb',['INES2_RAM_2KB',['../ines_8h.html#a8fee1f04d4a4d6e398df63e8f3374f42',1,'ines.h']]],
  ['ines2_5fram_5f32kb',['INES2_RAM_32KB',['../ines_8h.html#a27ec017f995f951d6c5ee344fbc79aaf',1,'ines.h']]],
  ['ines2_5fram_5f4kb',['INES2_RAM_4KB',['../ines_8h.html#a5b4dbaf2033e580bb79c669df819d258',1,'ines.h']]],
  ['ines2_5fram_5f512b',['INES2_RAM_512B',['../ines_8h.html#af360c9e81030d6c4c0a158bf3994584b',1,'ines.h']]],
  ['ines2_5fram_5f512kb',['INES2_RAM_512KB',['../ines_8h.html#a589783a78617a05857aa3573250f35cb',1,'ines.h']]],
  ['ines2_5fram_5f64kb',['INES2_RAM_64KB',['../ines_8h.html#a2500dc4e692d239b670243665e135bba',1,'ines.h']]],
  ['ines2_5fram_5f8kb',['INES2_RAM_8KB',['../ines_8h.html#ad16ef80bbe183ce9eb5532b7e360c2fb',1,'ines.h']]]
];
