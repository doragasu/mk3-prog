var searchData=
[
  ['ines',['ines',['../group__ines.html',1,'']]],
  ['ines_2ec',['ines.c',['../ines_8c.html',1,'']]],
  ['ines_2eh',['ines.h',['../ines_8h.html',1,'']]],
  ['ines2_5fram_5f0b',['INES2_RAM_0B',['../ines_8h.html#afd92900471f943256a7164c00e0ff489',1,'ines.h']]],
  ['ines2_5fram_5f1024kb',['INES2_RAM_1024KB',['../ines_8h.html#ae28fc1fb47b27fdef4436cf3a38ecac4',1,'ines.h']]],
  ['ines2_5fram_5f128b',['INES2_RAM_128B',['../ines_8h.html#a37978cc2d6584fbbcd1fd62e7aa1c8a2',1,'ines.h']]],
  ['ines2_5fram_5f128kb',['INES2_RAM_128KB',['../ines_8h.html#a66d1cb8d7c2c211e1d2de9ed13a1807e',1,'ines.h']]],
  ['ines2_5fram_5f16kb',['INES2_RAM_16KB',['../ines_8h.html#a59df2e77ce3c4d1c9af27ea773a8a123',1,'ines.h']]],
  ['ines2_5fram_5f1kb',['INES2_RAM_1KB',['../ines_8h.html#a0e173de3c215de71f26255a700510a78',1,'ines.h']]],
  ['ines2_5fram_5f256b',['INES2_RAM_256B',['../ines_8h.html#a7f748e3ef2be5546e1ac0694afe5ce45',1,'ines.h']]],
  ['ines2_5fram_5f256kb',['INES2_RAM_256KB',['../ines_8h.html#a38f7fd6dba34fb157f691487f9567db5',1,'ines.h']]],
  ['ines2_5fram_5f2kb',['INES2_RAM_2KB',['../ines_8h.html#a8fee1f04d4a4d6e398df63e8f3374f42',1,'ines.h']]],
  ['ines2_5fram_5f32kb',['INES2_RAM_32KB',['../ines_8h.html#a27ec017f995f951d6c5ee344fbc79aaf',1,'ines.h']]],
  ['ines2_5fram_5f4kb',['INES2_RAM_4KB',['../ines_8h.html#a5b4dbaf2033e580bb79c669df819d258',1,'ines.h']]],
  ['ines2_5fram_5f512b',['INES2_RAM_512B',['../ines_8h.html#af360c9e81030d6c4c0a158bf3994584b',1,'ines.h']]],
  ['ines2_5fram_5f512kb',['INES2_RAM_512KB',['../ines_8h.html#a589783a78617a05857aa3573250f35cb',1,'ines.h']]],
  ['ines2_5fram_5f64kb',['INES2_RAM_64KB',['../ines_8h.html#a2500dc4e692d239b670243665e135bba',1,'ines.h']]],
  ['ines2_5fram_5f8kb',['INES2_RAM_8KB',['../ines_8h.html#ad16ef80bbe183ce9eb5532b7e360c2fb',1,'ines.h']]],
  ['ines2_5fram_5fsize',['ines2_ram_size',['../group__ines.html#ga91bca43263b5e7d14c4c9b1257bea78d',1,'ines.h']]],
  ['ines_5ffill_5fprg_5fkb',['INES_FILL_PRG_KB',['../group__ines.html#ga98a1d92a27ec72b0613f0b1b8d233611',1,'ines.h']]],
  ['ines_5ffree',['ines_free',['../group__ines.html#ga07281a229e0b3eb50101d71c2112a0c6',1,'ines_free(struct ines_rom_data *data):&#160;ines.c'],['../group__ines.html#ga07281a229e0b3eb50101d71c2112a0c6',1,'ines_free(struct ines_rom_data *data):&#160;ines.c']]],
  ['ines_5fhead_5ftype',['ines_head_type',['../ines_8c.html#add04930cf988567cd4637acad7829c39',1,'ines.c']]],
  ['ines_5fheader',['ines_header',['../structines__header.html',1,'']]],
  ['ines_5fload',['ines_load',['../group__ines.html#ga40062f2485ba1b4c80fada393b0ee9db',1,'ines_load(const char *file, const struct ines_read_flags *flags, struct ines_rom_data *data):&#160;ines.c'],['../group__ines.html#ga40062f2485ba1b4c80fada393b0ee9db',1,'ines_load(const char *file, const struct ines_read_flags *flags, struct ines_rom_data *data):&#160;ines.c']]],
  ['ines_5fmapper',['ines_mapper',['../group__ines.html#ga995f7e352050310dccbd573e826b7127',1,'ines.h']]],
  ['ines_5fmapper_5ftable',['INES_MAPPER_TABLE',['../group__ines.html#ga44e35280710c963f460b360a53f4eb84',1,'ines.h']]],
  ['ines_5fread_5fflags',['ines_read_flags',['../structines__read__flags.html',1,'']]],
  ['ines_5from_5fdata',['ines_rom_data',['../structines__rom__data.html',1,'']]],
  ['ines_5fstat_5ffile_5fopen_5ferror',['INES_STAT_FILE_OPEN_ERROR',['../group__ines.html#ggaed85a1c07ec94cfe1ae011093cdf091ea89172ca10b716b2b2b6fba8cd6e81b4b',1,'ines.h']]],
  ['ines_5fstat_5ffile_5fread_5ferror',['INES_STAT_FILE_READ_ERROR',['../group__ines.html#ggaed85a1c07ec94cfe1ae011093cdf091eace291ae1ba9dd2038f05fb5f692b3e02',1,'ines.h']]],
  ['ines_5fstat_5fformat_5ferror',['INES_STAT_FORMAT_ERROR',['../group__ines.html#ggaed85a1c07ec94cfe1ae011093cdf091ea03c988a5bd754ad87cf87d5165f32264',1,'ines.h']]],
  ['ines_5fstat_5fmemory_5ferror',['INES_STAT_MEMORY_ERROR',['../group__ines.html#ggaed85a1c07ec94cfe1ae011093cdf091eac24df0e6d6b91319e780ac2bb40758ee',1,'ines.h']]],
  ['ines_5fstat_5fok',['INES_STAT_OK',['../group__ines.html#ggaed85a1c07ec94cfe1ae011093cdf091ea2ad3566c98b1f578658fd2a2f1a99d7f',1,'ines.h']]],
  ['ines_5fstat_5funspecified_5ferror',['INES_STAT_UNSPECIFIED_ERROR',['../group__ines.html#ggaed85a1c07ec94cfe1ae011093cdf091ea07111fab078a2c15241c77d0059b8e19',1,'ines.h']]],
  ['ines_5fstatus',['ines_status',['../group__ines.html#gaed85a1c07ec94cfe1ae011093cdf091e',1,'ines.h']]],
  ['ines_5fto_5fmapper_5fnumber',['ines_to_mapper_number',['../group__ines.html#gaff70ead067ff229084fd2ea11735a1f5',1,'ines_to_mapper_number(enum ines_mapper mapper):&#160;ines.c'],['../group__ines.html#gaff70ead067ff229084fd2ea11735a1f5',1,'ines_to_mapper_number(enum ines_mapper mapper):&#160;ines.c']]],
  ['ines_5fto_5fmapper_5fstr',['ines_to_mapper_str',['../group__ines.html#ga74d5a75be614dd2cfc1c373f0675d105',1,'ines_to_mapper_str(enum ines_mapper mapper):&#160;ines.c'],['../group__ines.html#ga74d5a75be614dd2cfc1c373f0675d105',1,'ines_to_mapper_str(enum ines_mapper mapper):&#160;ines.c']]],
  ['ines_5ftrainer_5flength',['INES_TRAINER_LENGTH',['../ines_8c.html#af63cb0b1b000056137fa200e4b8b62ee',1,'ines.c']]],
  ['ines_5ftv_5fsystem_5fdual1',['INES_TV_SYSTEM_DUAL1',['../group__ines.html#gga99fb83031ce9923c84392b4e92f956b5aa415a3b78690d78dc869c03ed9158131',1,'ines.h']]],
  ['ines_5ftv_5fsystem_5fdual2',['INES_TV_SYSTEM_DUAL2',['../group__ines.html#gga99fb83031ce9923c84392b4e92f956b5ab2f68be6920fe99c3bed6d3f7624f96a',1,'ines.h']]],
  ['ines_5ftv_5fsystem_5fntsc',['INES_TV_SYSTEM_NTSC',['../group__ines.html#gga99fb83031ce9923c84392b4e92f956b5ac17b0049acd446c170e634b791a68b68',1,'ines.h']]],
  ['ines_5ftv_5fsystem_5fpal',['INES_TV_SYSTEM_PAL',['../group__ines.html#gga99fb83031ce9923c84392b4e92f956b5aa01f16e7e504d8e512a678332a4690a7',1,'ines.h']]],
  ['ines_5ftype_5farchaic_5fines',['INES_TYPE_ARCHAIC_INES',['../ines_8c.html#add04930cf988567cd4637acad7829c39a7e214bacbb495b5a10029a5d420bb6c1',1,'ines.c']]],
  ['ines_5ftype_5fines',['INES_TYPE_INES',['../ines_8c.html#add04930cf988567cd4637acad7829c39a54c46fe8ec1f86c0aecf90448ddc6f39',1,'ines.c']]],
  ['ines_5ftype_5fnes2',['INES_TYPE_NES2',['../ines_8c.html#add04930cf988567cd4637acad7829c39af93557e749a863eb6c81a87e9591d97d',1,'ines.c']]],
  ['ines_5fx_5fmacro_5fas_5fenum',['INES_X_MACRO_AS_ENUM',['../group__ines.html#gaa6bd55533a69421102ba4a9df41a3c8c',1,'ines.h']]],
  ['ines_5fx_5fmacro_5fas_5fmapper_5fnumber',['INES_X_MACRO_AS_MAPPER_NUMBER',['../ines_8c.html#a694451c026c6abf2f08710fe2ce547b1',1,'ines.c']]],
  ['ines_5fx_5fmacro_5fas_5fmapper_5fstr',['INES_X_MACRO_AS_MAPPER_STR',['../ines_8c.html#a7d8a341da6acdc71a23479a7793cbe72',1,'ines.c']]]
];
