/************************************************************************//**
 * \file
 *
 * \brief Flash Manager.
 *
 * Handles basic operations on flash chips (Program/Read/Erase) using MDMA
 * interface.
 *
 * \author doragasu
 * \date   2017
 ****************************************************************************/
#include <QApplication>
#include <stdlib.h>
#include "flash_man.h"
#include "util.h"

int FlashMan::Write(CmdChip chip, uint8_t *buf, bool autoErase,
		uint32_t start, uint32_t len)
{
	uint32_t addr;
	int toWrite;
	uint32_t i;

	// If requested, perform auto-erase
	if (autoErase && (chip == CMD_CHIP_CHR_FLASH ||
                chip == CMD_CHIP_PRG_FLASH)) {
		emit StatusChanged("Auto erasing");
		QApplication::processEvents();
		DelayMs(1);
		if (CmdRangeErase(chip, start, len)) {
			return -1;
		}
	}

	emit RangeChanged(0, len);
	emit ValueChanged(0);
	emit StatusChanged("Programming");
	QApplication::processEvents();

	for (i = 0, addr = start; i < len;) {
		toWrite = MIN(32768, len - i);
		if (CmdWrite(chip, buf + i, addr, toWrite)) {
			return -1;
		}
		// Update vars and draw progress bar
		i += toWrite;
		addr += toWrite;
		emit ValueChanged(i);
		QApplication::processEvents();
	}
	emit ValueChanged(i);
	emit StatusChanged("Done!");
	QApplication::processEvents();
	return 0;
}

uint8_t *FlashMan::Read(CmdChip chip, uint32_t start, uint32_t len)
{
	uint8_t *readBuf;
	int toRead;
	uint32_t addr;
	uint32_t i;

	emit RangeChanged(0, len);
	emit ValueChanged(0);
	emit StatusChanged("Reading");
	QApplication::processEvents();

	readBuf = (uint8_t*)malloc(len);
	if (!readBuf) {
		return NULL;
	}

	for (i = 0, addr = start; i < len;) {
		toRead = MIN(32768, len - i);
		if (CmdRead(chip, readBuf + i, addr, toRead)) {
			free(readBuf);
			return NULL;
		}
		// Update vars and draw progress bar
		i += toRead;
		addr += toRead;
		emit ValueChanged(i);
		QApplication::processEvents();
	}
	emit ValueChanged(i);
	emit StatusChanged("Done");
	QApplication::processEvents();
	return readBuf;
}

int FlashMan::RangeErase(CmdChip chip, uint32_t start, uint32_t len)
{
	if (CmdRangeErase(chip, start, len)) {
        return -1;
    }

	return 0;
}

int FlashMan::FullErase(CmdChip chip)
{
	if (CmdFlashErase(chip, CMD_ERASE_FULL)) {
        return -1;
    }

	return 0;
}

void FlashMan::BufFree(uint8_t *buf)
{
	free(buf);
}

uint8_t FlashMan::FlashIdsGet(CmdFlashId id[2])
{
	return CmdFIdGet(id);
}

int FlashMan::DfuBootloader(void)
{
	return CmdBootloader();
}

int FlashMan::ProgFwGet(uint16_t *fwVer)
{
    return CmdFwGet(fwVer);
}

int FlashMan::MapperSet(int type)
{
    return CmdMapperCfg(type);
}

int FlashMan::MapperProgram(int type, int vMirror)
{
	emit StatusChanged("Burn mapper");
	QApplication::processEvents();
    return CmdMapperProgram(type, vMirror);
}

