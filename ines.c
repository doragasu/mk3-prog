/************************************************************************//** * \file
 *
 * \brief iNES/NES2.0 file parser.
 *
 * \author doragasu
 * \date   2018
 ****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "util.h"
#include "ines.h"

/// String for NO
#define _NO "no"
/// String for YES
#define _YES "yes"

/// Length of the trainer block
#define INES_TRAINER_LENGTH     512

/// Expand mapper information table as mapper string (names)
#define INES_X_MACRO_AS_MAPPER_STR(num, name, str)      \
    str,

/// Mapper names table
static const char *ines_mapper_str[INES_MAPPER_MAX] = {
    INES_MAPPER_TABLE(INES_X_MACRO_AS_MAPPER_STR)
};

/// Expand mapper information table as mapper numbers
#define INES_X_MACRO_AS_MAPPER_NUMBER(num, name, str)   \
    num,

/// Mapper numbers table
static const uint16_t ines_mapper_number[INES_MAPPER_MAX] = {
    INES_MAPPER_TABLE(INES_X_MACRO_AS_MAPPER_NUMBER)
};

/// iNES/NES2.0 header type
enum ines_head_type {
    INES_TYPE_ARCHAIC_INES, ///< Archaic iNES
    INES_TYPE_INES,         ///< iNES
    INES_TYPE_NES2          ///< NES 2.0
};

static void ines_print(struct ines_header *hdr);
static void ines_fill(uint8_t *rom, uint32_t written, uint32_t fill);
static enum ines_mapper ines_find_mapper(uint16_t mapper_num);
static enum ines_head_type ines_head_type(struct ines_header *head);
static uint16_t ines_mapper_num(struct ines_header *hdr,
        enum ines_head_type type);
static uint16_t ines_chr_rom_len_blocks_8k(struct ines_header *hdr,
        enum ines_head_type type);
static uint16_t ines_prg_rom_blocks_16k(struct ines_header *hdr,
        enum ines_head_type type);

enum ines_status ines_load(const char *file,
        const struct ines_read_flags *flags,
        struct ines_rom_data *data)
{
    FILE *rom = NULL;
    uint16_t blocks;
    enum ines_head_type type;
    enum ines_status err = INES_STAT_UNSPECIFIED_ERROR;

    memset(data, 0, sizeof(struct ines_rom_data));

    // Open file and read header
    if (!(rom = fopen(file, "rb"))) {
        return INES_STAT_FILE_OPEN_ERROR;
    }
    if (fread(&data->header, sizeof(data->header), 1, rom) != 1) {
        err = INES_STAT_FILE_READ_ERROR;
        goto error;
    }
    // Check magic
    if (data->header.magic[0] != 'N' || data->header.magic[1] != 'E' ||
            data->header.magic[2] != 'S' || data->header.magic[3] != 0x1A) {
        err = INES_STAT_FORMAT_ERROR;
        goto error;
    }

    // Print header info
    if (flags && flags->verbose) {
        ines_print(&data->header);
    }

    type = ines_head_type(&data->header);

    // Search mapper
    data->mapper_num = ines_mapper_num(&data->header, type);

    // Skip trainer if present
    if (data->header.trainer) {
        if (fseek(rom, INES_TRAINER_LENGTH, SEEK_CUR) < 0) {
            err = INES_STAT_FILE_READ_ERROR;
            goto error;
        }
    }

    // Read PRG ROM
    blocks = ines_prg_rom_blocks_16k(&data->header, type);
    if (flags && flags->fill_prg_16k_blocks) {
        blocks = MAX(blocks, flags->fill_prg_16k_blocks);
    }
    data->prg_rom_16k_blocks = blocks;
    data->prg_rom = (uint8_t*)malloc(16 * 1024 * blocks);
    if (!data->prg_rom) {
        err = INES_STAT_MEMORY_ERROR;
        goto error;
    }
    if (fread(data->prg_rom, 16 * 1024, data->header.prg_16k_blocks, rom) !=
            data->header.prg_16k_blocks) {
        err = INES_STAT_FILE_READ_ERROR;
        goto error;
    }
    ines_fill(data->prg_rom, 16 * 1024 * data->header.prg_16k_blocks,
            16 * 1024 * data->prg_rom_16k_blocks);

    // Read CHR ROM
    blocks = ines_chr_rom_len_blocks_8k(&data->header, type);
    if (flags && flags->fill_chr_8k_blocks) {
        blocks = MAX(blocks, flags->fill_chr_8k_blocks);
    }
    data->chr_rom_8k_blocks = blocks;
    data->chr_rom = (uint8_t*)malloc(8 * 1024 * blocks);
    if (!data->chr_rom) {
        err = INES_STAT_MEMORY_ERROR;
        goto error;
    }
    if (fread(data->chr_rom, 8 * 1024, data->header.chr_8k_blocks, rom) !=
            data->header.chr_8k_blocks) {
        err = INES_STAT_FILE_READ_ERROR;
        goto error;
    }
    ines_fill(data->chr_rom, 8 * 1024 * data->header.chr_8k_blocks,
            8 * 1024 * data->chr_rom_8k_blocks);

    // Everything OK!
    fclose(rom);
    return INES_STAT_OK;

error:
    ines_free(data);
    if (rom) fclose(rom);
    return err;
}

void ines_free(struct ines_rom_data *data) {
    if (data && data->chr_rom) {
        free(data->chr_rom);
        data->chr_rom = NULL;
    }
    if (data && data->prg_rom) {
        free(data->prg_rom);
        data->prg_rom = NULL;
    }
}

/************************************************************************//**
 * \brief Fills the memory to the specified length, with copies from the input
 * data.
 *
 * \param[inout] rom  Memory region to fill.
 * \param[in] written Bytes written into rom.
 * \param[in] fill    Length to fill up to.
 *
 * \warning: assuming written is a power of 2
 ****************************************************************************/
static void ines_fill(uint8_t *rom, uint32_t written, uint32_t fill)
{
    uint32_t i;

    for (i = 0; written < fill; written++, i++) {
        rom[written] = rom[i];
    }
}

/************************************************************************//**
 * \brief Prints header data in human readable format.
 *
 * \param[in] hdr Header to print info from.
 ****************************************************************************/
static void ines_print(struct ines_header *hdr) {
    enum ines_mapper mapper;
    enum ines_head_type type;
    uint16_t mapper_num;

    if (hdr->magic[0] == 'N' && hdr->magic[1] == 'E' &&
            hdr->magic[2] == 'S' && hdr->magic[3] == 0x1A) {
        printf("Header MAGIC OK!\n");
    } else {
        printf("Header MAGIC NOT OK!\n");
    }
    type = ines_head_type(hdr);
    printf("Header type: ");
    switch (type) {
        case INES_TYPE_ARCHAIC_INES:
            puts("archaic iNES");
            break;

        case INES_TYPE_INES:
            puts("iNES");
            break;

        case INES_TYPE_NES2:
            puts("NES 2.0");
            break;
    }
    mapper_num = ines_mapper_num(hdr, type);
    mapper = ines_find_mapper(mapper_num);
    printf("Mapper: %u (%s)\n", mapper_num, ines_to_mapper_str(mapper));
    printf("PRG length (KiB): %u\n", 16 * ines_prg_rom_blocks_16k(hdr, type));
    printf("CHR length (KiB): %u\n", 8 * ines_chr_rom_len_blocks_8k(hdr, type));
    if (hdr->mirror_ctl_ignore) {
        printf("Four-screen VRAM\n");
    } else {
        printf("Mirroring: %s\n", hdr->mirror_vert?"vertical":"horizontal");
    }
    if (hdr->trainer) printf("Trainer present\n");
    if (hdr->saver_mem) printf("Saver support\n");
    if (hdr->playchoice10) printf("PlayChoice-10\n");
    if (hdr->vs_unisystem) printf("VS Unisystem\n");

    if (INES_TYPE_INES == type) {
        // Non archaic INES fields
        printf("Zone: %s\n", hdr->ines_flags9.pal?"PAL":"NTSC");
        switch (hdr->ines_flags10.tv_system) {
            case INES_TV_SYSTEM_NTSC:
                printf("  * NTSC only\n");
                break;
    
            case INES_TV_SYSTEM_PAL:
                printf("  * PAL only\n");
                break;
    
            case INES_TV_SYSTEM_DUAL1:
            case INES_TV_SYSTEM_DUAL2:
                printf("  * NTSC/PAL compatible\n");
    
        }
        if (hdr->ines_flags10.prg_ram) printf("PRG RAM: %d KiB\n",
                8 * hdr->ines_prg_ram_8k_blocks);
        if (hdr->ines_flags10.bus_conflict) printf("Cart has bus conflicts\n");
    } else if (INES_TYPE_NES2 == type) {
        // NES 2.0 fields
        if (hdr->nes2_flags8.submapper) {
            printf("Submapper %d\n", hdr->nes2_flags8.submapper);
        }
        if (hdr->nes2_flags10.saver_prg_ram_len) {
            printf("Save PRG RAM: %d bytes\n",
                    64<<hdr->nes2_flags10.saver_prg_ram_len);
        }
        if (hdr->nes2_flags10.work_prg_ram_len) {
            printf("Work PRG RAM: %d bytes\n",
                    64<<hdr->nes2_flags10.work_prg_ram_len);
        }
        if (hdr->nes2_flags11.saver_chr_ram_len) {
            printf("Save CHR RAM: %d bytes\n",
                    64<<hdr->nes2_flags11.saver_chr_ram_len);
        }
        if (hdr->nes2_flags11.work_chr_ram_len) {
            printf("Work CHR RAM: %d bytes\n",
                    64<<hdr->nes2_flags11.work_chr_ram_len);
        }
        printf("Region: ");
        if (hdr->nes2_flags12.both) {
            puts("both");
        } else if (hdr->nes2_flags12.pal) {
            puts("PAL");
        } else {
            puts("NTSC");
        }
    }
}

uint16_t ines_to_mapper_number(enum ines_mapper mapper) {
    if (mapper >= INES_MAPPER_MAX) {
        return UINT16_MAX;
    }
    return ines_mapper_number[mapper];
}

const char *ines_to_mapper_str(enum ines_mapper mapper) {
    if (mapper >= INES_MAPPER_MAX) {
        return NULL;
    }
    return ines_mapper_str[mapper];
}

/************************************************************************//**
 * \brief Finds mapper number into available mappers database. Returns the
 * corresponding mapper type.
 *
 * \param[in] maper_num mapper number to search in database.
 *
 * \return Mapper type index from mapper database.
 ****************************************************************************/
static enum ines_mapper ines_find_mapper(uint16_t mapper_num)
{
    enum ines_mapper mapper;

    for (mapper = 0; (mapper < INES_MAPPER_MAX) &&
            (mapper_num != ines_mapper_number[mapper]); mapper++);
    return mapper;
}

/************************************************************************//**
 * \brief Computes the mapper number from the header, considering the header
 * type.
 *
 * \param[in] hdr  iNES/NES2.0 header.
 * \param[in] type Header type
 *
 * \return The computed mapper number.
 ****************************************************************************/
static uint16_t ines_mapper_num(struct ines_header *hdr,
        enum ines_head_type type)
{
    uint16_t mapper_num = hdr->mapper_low;

    switch (type) {
        case INES_TYPE_NES2:
            mapper_num |= hdr->nes2_flags8.mapper_upper<<8;
            // fallthrough

        case INES_TYPE_INES:
            mapper_num |= hdr->mapper_high<<4;

        default:
            break;
    }

    return mapper_num;
}

/************************************************************************//**
 * \brief Parses the header to obtain the header type.
 *
 * \param[in] hdr  iNES/NES2.0 header.
 *
 * \return The computed header type.
 ****************************************************************************/
static enum ines_head_type ines_head_type(struct ines_header *hdr)
{
    if (2 == hdr->nes2_format) {
        return INES_TYPE_NES2;
    } else if (!hdr->nes2_format && !hdr->flags12 && !hdr->flags13 &&
            !hdr->flags14 && !hdr->zero) {
        return INES_TYPE_INES;
    } else {
        return INES_TYPE_ARCHAIC_INES;
    }
}

/************************************************************************//**
 * \brief Computes the number of available 8kiB blocks from CHR ROM.
 *
 * \param[in] hdr  iNES/NES2.0 header.
 * \param[in] type Header type
 *
 * \return The computed number of 8 kiB blocks of CHR ROM.
 ****************************************************************************/
static uint16_t ines_chr_rom_len_blocks_8k(struct ines_header *hdr,
        enum ines_head_type type)
{
    uint16_t blocks;

    blocks = hdr->chr_8k_blocks;
    if (INES_TYPE_NES2 == type) {
        blocks += hdr->nes2_flags9.chr_8k_blocks_high<<8;
    }

    return blocks;
}

/************************************************************************//**
 * \brief Computes the number of available 16kiB blocks from PRG ROM.
 *
 * \param[in] hdr  iNES/NES2.0 header.
 * \param[in] type Header type
 *
 * \return The computed number of 16 kiB blocks of PRG ROM.
 ****************************************************************************/
static uint16_t ines_prg_rom_blocks_16k(struct ines_header *hdr,
        enum ines_head_type type)
{
    uint16_t blocks;

    blocks = hdr->prg_16k_blocks;
    if (INES_TYPE_NES2 == type) {
        blocks += hdr->nes2_flags9.prg_16k_blocks_high<<8;
    }

    return blocks;
}

//int main(int argc, char **argv) {
//    struct ines_read_flags flags = {};
//    struct ines_rom_data data;
//    enum ines_status status;
//
//    if (argc != 2) {
//        puts("Argument must be a single NES ROM");
//        return 1;
//    }
//    
//    flags.verbose = TRUE;
//    status = ines_load(argv[1], &flags, &data);
//    if (INES_STAT_OK != status) {
//        printf("FAIL! CODE: %d\n", status);
//    } else {
//        puts("SUCCESS!");
//    }
//
//    ines_free(&data);
//
//    return 0;
//}

