# Mojo-NES MK3 programmer Command Line Interface
Awesome Mojo-NES MKIII programmer. The program can be used via command line, and via Qt GUI.

This utility allows to manage mojo-nes-mk3 cartridges, using a mojo-nes-mk3 programmer. The utility allows to program and read flash and RAM chips, burn the firmware for the CIC chip and burn supported mappers to the FPGA. A driver system allows adding your own mappers to be configured and downloaded to the cartridge.

# Building
It is possible to build the binary without Qt support (with only the CLI mode available) or with Qt support (both CLI and GUI modes available). The build process has been tested for Linux and Windows. It should also work with macos, but I have no way of testing it. If you give it a try under macos, please drop me a line about the process.

You will need a working GNU GCC compiler and an Awesome Mojo-NES MKIII programmer to burn the ROM to a cartridge. You will also need to install `libglib-2` and `libxml2` libraries, including development headers. If you go for the full-fledged gui, you will need to install the `qt5-base` development packages (`qt5-default` in Ubuntu and derivatives) along with development headers.

## Basic CLI
Once you have the dependencies installed, just run:
```
$ make -f Makefile-no-qt
```
If everything goes OK, you should have the `mk3-prog` binary sitting in the same directory.

## Full-featured GUI + CLI
Once the dependencies are installed, run:
```
$ qt5-qmake
$ make
```
If the build process completes successfully, you should be able to run `mk3-prog -Q` to start the GUI.

# Usage
Once you have plugged a Mojo-NES MKIII cartridge into an Awesome Mojo-NES MKIII Programmer, you can use mk3-prog. The command line application invocation must be as follows:
```
$ mk3-prog [option1 [option1_arg]] […] [optionN [optionN_arg]]
```
The options (option1 ~ optionN) can be any combination of the ones listed below. Options must support short and long formats. Depending on the used option, and option argument (option\_arg) must be supplied. Several options can be used on the same command, as long as the combination makes sense (e.g. it does make sense using the flash and verify options together, but using the help option with the flash option doesn't make too much sense).

| Option | Description |
|---|---|
| -Q, --qt-gui | Launch Qt GUI |
| -f, --firm-ver | Get programmer firmware version |
| -n, --flash-nes | Flash iNES (\*.nes) ROM. |
| -c, --flash-chr \<arg\> | Flash file to CHR ROM |
| -p, --flash-prg \<arg\> | Flash file to PRG ROM |
| -C, --read-chr \<arg\> | Read CHR ROM to file |
| -P, --read-prg \<arg\> | Read PRG ROM to file |
| -e, --erase-chr | Erase CHR Flash |
| -E, --erase-prg | Erase PRG Flash |
| -s, --esect-chr \<arg\> | Erase CHR flash sector |
| -S, --esect-prg \<arg\> | Erase PRG flash sector |
| -g, --erange-chr \<arg\> | Erase CHR flash memory range |
| -G, --erange-prg \<arg\> | Erase PRG flash memory range |
| -A, --erase-auto | Automatically erase required flash range |
| -V, --verify | Verify flash after writing file |
| -i, --flash-id | Obtain flash chips identifiers |
| -R, --read-ram \<arg\> | Read data from RAM chip |
| -W, --write-ram \<arg\> | Write data to RAM chip |
| -b, --fpga-flash \<arg\> | Upload bitfile to FPGA, using .xcf file |
| -a, --cic-flash \<arg\> | AVR CIC firmware flash |
| -F, --firm-flash \<arg\> | Flash programmer firmware |
| -M, --mapper \<arg\> | Set mapper: 1-NOROM, 2-MMC3, 3-NFROM |
| -B, --bootloader | Enter bootloader mode |
| -d, --dry-run | Dry run: don't actually do anything |
| -r, --version | Show program version |
| -v, --verbose | Show additional information |
| -h, --help | Print help screen and exit |

The \<arg\> text indicates that the option takes an input argument. For the options requiring an argument that represents a memory image file, or a memory address, the syntax is as follows:

* Memory image file: file\_name[:start\_addr[:length]]. Along with the file name, optional address and length fields can be added, separated by the colon (:) character, resulting in the following format:
* Address: Specifies an address related to the command (e.g. the address to which to flash a cartridge ROM or WiFi firmware blob).

Some examples of the command invocation and its arguments are:

* `$ mk3-prog -VeEc chr_rom_file -p prg_rom_file` → Erases entire cartridge (both CHR and PRG flash chips), flashes chr\_rom\_file to CHR flash, prg\_rom\_file to PRG flash, and verifies the writes.
* `$ mk3-prog --erase_chr -c chr_rom_file:0x1000` → Erases entire CHR flash chip and flashes contents of chr\_rom\_file to CHR flash, starting at address 0x1000.
* `$ mk3-prog -S 0x10000` → Erases PRG flash sector containing 0x100000 address.
* `$ mk3-prog -Vp prg_rom_file:0x10000:32768` → Flashes 32 KiB of prg\_rom\_file to address 0x10000, and verifies the operation.
* `$ mk3-prog --read_chr chr_rom_file::1048576` → Reads 1 MiB of the CHR flash chip, and writes it to chr\_rom\_file. Note that if you want to specify length but do not want to specify address, you have to use two colon characters before length. This way, missing address argument is interpreted as 0.

# Authors
This program has been written by doragasu.

# Contributions
Contributions are welcome. If you find a bug please open an issue, and if you have implemented a cool feature/improvement, please send a pull request.

# License
This program is provided with NO WARRANTY, under the [GPLv3 license](https://www.gnu.org/licenses/gpl-3.0.html).
