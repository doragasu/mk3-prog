var searchData=
[
  ['ines_5ffree',['ines_free',['../group__ines.html#ga07281a229e0b3eb50101d71c2112a0c6',1,'ines_free(struct ines_rom_data *data):&#160;ines.c'],['../group__ines.html#ga07281a229e0b3eb50101d71c2112a0c6',1,'ines_free(struct ines_rom_data *data):&#160;ines.c']]],
  ['ines_5fload',['ines_load',['../group__ines.html#ga40062f2485ba1b4c80fada393b0ee9db',1,'ines_load(const char *file, const struct ines_read_flags *flags, struct ines_rom_data *data):&#160;ines.c'],['../group__ines.html#ga40062f2485ba1b4c80fada393b0ee9db',1,'ines_load(const char *file, const struct ines_read_flags *flags, struct ines_rom_data *data):&#160;ines.c']]],
  ['ines_5fto_5fmapper_5fnumber',['ines_to_mapper_number',['../group__ines.html#gaff70ead067ff229084fd2ea11735a1f5',1,'ines_to_mapper_number(enum ines_mapper mapper):&#160;ines.c'],['../group__ines.html#gaff70ead067ff229084fd2ea11735a1f5',1,'ines_to_mapper_number(enum ines_mapper mapper):&#160;ines.c']]],
  ['ines_5fto_5fmapper_5fstr',['ines_to_mapper_str',['../group__ines.html#ga74d5a75be614dd2cfc1c373f0675d105',1,'ines_to_mapper_str(enum ines_mapper mapper):&#160;ines.c'],['../group__ines.html#ga74d5a75be614dd2cfc1c373f0675d105',1,'ines_to_mapper_str(enum ines_mapper mapper):&#160;ines.c']]]
];
