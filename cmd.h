/************************************************************************//**
 * \file
 * \brief Allows sending commands to the programmer, and receiving results.
 *
 * \defgroup cmd cmd
 * \{
 * \brief Allows sending commands to the programmer, and receiving results.
 *
 * \author Jesus Alonso (doragasu)
 * \date   2016
 ****************************************************************************/
#ifndef _CMD_H_
#define _CMD_H_

#include <stdint.h>

/** \addtogroup CmdRet
 *  \brief Return values for functions in this module and error codes.
 *  \{ */
#define CMD_OK		 0		///< Function completed successfully
#define CMD_ERROR	-1		///< Function completed with error
/** \} */

/// Used to erase a full flash chip
#define CMD_ERASE_FULL	0xFFFFFF

/// Timeout for regular USB commands (ms)
#define CMD_TOUT_MS         3000

/// Timeout for slow USB commands (ms)
#define CMD_TOUT_LONG_MS    70000

/// Maximum length of a command
#define CMD_MAXLEN	64

/// Maximum SRAM length is 8 KiB
#define CMD_SRAM_MAXLEN		8*1024

/// Memory chip enumeration
enum CmdChip {
    CMD_CHIP_CHR_FLASH = 0, ///< CHR flash chip.
    CMD_CHIP_PRG_FLASH,     ///< PRG flash chip.
    CMD_CHIP_PRG_RAM,       ///< RAM chip.
    CMD_CHIP_MAX            ///< Number of supported chips.
};

/// Contains the data relative to a single mapper
struct CmdMapperData {
    const char *name;       ///< Mapper name
    int mirror_variant;     ///< Mapper has mirroring variants when true
    uint16_t number;        ///< Mapper number`
};

/// Supported system commands. Also includes OK and error codes.
typedef enum {
	CMD_REP_OK = 0,			///< OK reply code
	CMD_FW_VER,				///< Get programmer firmware version
    CMD_BOOTLOADER,         ///< Enter bootloader mode
	CMD_CHR_WRITE,			///< Write to CHR flash
	CMD_PRG_WRITE,			///< Write to PRG flash
	CMD_CHR_READ,			///< Read from CHR flash
	CMD_PRG_READ,			///< Read from PRG flash
	CMD_CHR_ERASE,			///< Erase CHR flash (entire or sectors)
	CMD_PRG_ERASE,			///< Erase PRG flash (entire or sectors)
	CMD_FLASH_ID,			///< Get flash chips identifiers
	CMD_RAM_WRITE,  		///< Write data to cartridge SRAM
	CMD_RAM_READ,			///< Read data from cartridge SRAM
	CMD_MAPPER_SET, 		///< Configure cartridge mapper
	CMD_CHR_RANGE_ERASE,	///< Erase a memory range from CHR flash
	CMD_PRG_RANGE_ERASE,	///< Erase a memory range from PRG flash
	CMD_REP_ERROR = 255		///< Error reply code
} SysCmdCode;

/// Command header for the mapper set command.
typedef struct {
    uint8_t cmd;            ///< Command code
    uint8_t pad;            ///< Padding
    uint16_t mapper;        ///< Mapper to set
} CmdMapperSet;

/// Command header for memory read and write commands.
typedef struct {
	uint8_t cmd;		///< Command code
	uint8_t addr[3];	///< Address to read/write
	uint8_t len[2];		///< Length to read/write
} CmdRdWrHdr;

/// Command header for erase commands.
typedef struct {
	uint8_t cmd;			///< Command code
	uint8_t sectAddr[3];	///< Address to erase, Full chip if 0xFFFFFF
} CmdErase;

/// Command header for range erase commands
typedef struct {
    uint8_t cmd;        ///< Command code
    uint8_t start[3];   ///< Initial address
    uint8_t len[3];     ///< Memory range length
    uint8_t pad;        ///< Unused
} CmdRngErase;

/// Generic command request.
typedef union {
	uint8_t data[CMD_MAXLEN];	///< Raw data (32 bytes max)
	uint8_t command;			///< Command code
	CmdRdWrHdr rdWr;			///< Read/write request
	CmdErase erase;				///< Erase request
    CmdRngErase rangeErase;   ///< Erase memory range request
    CmdMapperSet mapperSet;     ///< Set mapper type
} Cmd;

/// Flash chip identification information.
typedef struct {
	uint8_t manId;			///< Manufacturer ID
	uint8_t devId[3];		///< Device ID
} CmdFlashId;

/// Empty command response.
typedef struct {
	uint8_t code;			///< Response code (OK/ERROR)
} CmdRepEmpty;

/// Firmware version command response.
typedef struct {
	uint8_t code;			///< Response code (OK/ERROR)
	uint8_t ver_major;		///< Major version number
	uint8_t ver_minor;		///< Minor version number
} CmdRepFwVer;

/// Flash ID command response.
typedef struct {
	uint8_t code;			///< Command code
	uint8_t pad;			///< Padding
	CmdFlashId prg;			///< PRG Flash information
	CmdFlashId chr;			///< CHR Flash information
} CmdRepFlashId;

/// Generic reply to a command request.
typedef union {
	uint8_t data[CMD_MAXLEN];		///< Raw data (up to 32 bytes)
	uint8_t command;		///< Command code
	CmdRepEmpty eRep;		///< Empty command response
	CmdRepFlashId fId;		///< Flash ID command response
	CmdRepFwVer fwVer;		///< Firmware version command response
} CmdRep;

#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************//**
 * Module initialization. Call before using any other function.
 *
 * \param[in] deviceId    VID (upper 16 bits) and PID (lower 16 bits) of the
 * 			  programmer.
 * \param[in] num_mappers Number of supported mappers.
 * \param[in] mapper      Mapper data for the supported mappers.
 *
 * \return CMD_OK if the command completed successfully. CMD_ERROR otherwise.
 ****************************************************************************/
int CmdInit(uint32_t deviceId, int num_mappers,
        const struct CmdMapperData *mapper);

/************************************************************************//**
 * Sends a command, and obtains the command response.
 *
 * \param[in]  cmd Command to send.
 * \param[in]  cmdLen Command length.
 * \param[out] rep Response to the sent command.
 * \param[in]  timeout_ms Reply timeout in milliseconds
 *
 * \return CMD_OK if the command completed successfully. CMD_ERROR otherwise.
 * \note This function does not allow sending or receiving commands with long
 * payloads. Use CmdSendLongCmd() or CmdSendLongRep() for long payloads.
 ****************************************************************************/
int CmdSend(const Cmd *cmd, uint8_t cmdLen, CmdRep *rep, int timeout_ms);

/************************************************************************//**
 * Sends a command with a long data payload, and obtains the command response.
 *
 * \param[in]  cmd Command to send.
 * \param[in]  cmdLen Command length.
 * \param[in]  data Long data payload to send.
 * \param[in]  dataLen Length of the data payload.
 * \param[out] rep Response to the sent command.
 * \param[in]  timeout_ms Reply timeout in milliseconds
 *
 * \return CMD_OK if the command completed successfully. CMD_ERROR otherwise.
 ****************************************************************************/
int CmdSendLongCmd(const Cmd *cmd, uint8_t cmdLen, const uint8_t *data,
				   int dataLen, CmdRep *rep, int timeout_ms);

/************************************************************************//**
 * Sends a command requiring a long response payload.
 *
 * \param[in]  cmd        Command to send.
 * \param[in]  cmdLen     Command length.
 * \param[out] rep        Response to the sent command.
 * \param[in]  data       Long data payload to receive.
 * \param[in]  recvLen    Length of payload to receive.
 * \param[in]  timeout_ms Timeout in milliseconds for the operation.
 *
 * \return Length of the received payload if OK, CMD_ERROR otherwise.
 ****************************************************************************/
int CmdSendLongRep(const Cmd *cmd, uint8_t cmdLen, CmdRep *rep,
				   uint8_t *data, int recvLen, int timeout_ms);

/************************************************************************//**
 * \brief Enter bootloader mode.
 *
 * \return 0 on success, less than 0 on error.
 ****************************************************************************/
int CmdBootloader(void);

/************************************************************************//**
 * \brief Obtain flash chip identifiers of the inserted cart.
 *
 * \param[out] id Identifiers of the flash chips.
 *
 * \return 0 on success, less than 0 on error.
 ****************************************************************************/
int CmdFIdGet(CmdFlashId id[2]);

/************************************************************************//**
 * \brief Erases specified flash sector, or complete flash if addr is set
 * to PROG_ERASE_FULL.
 *
 * \param[in] chip Flash chip to erase.
 * \param[in] addr Address of the sector to erase.
 *
 * \return 0 on success, less than 0 on error.
 ****************************************************************************/
int CmdFlashErase(enum CmdChip chip, uint32_t addr);

/************************************************************************//**
 * \brief Erases specified flash memory range.
 *
 * \param[in] chip   Flash chip to erase.
 * \param[in] addr   Address to start erasing from.
 * \param[in] length Length of the block to erase.
 *
 * \return 0 on success, less than 0 on error.
 ****************************************************************************/
int CmdRangeErase(enum CmdChip chip, uint32_t addr, uint32_t length);

/************************************************************************//**
 * Write data to memory chip.
 *
 * \param[in] chip Chip to write data to.
 * \param[in] buf  Data to write to the chip.
 * \param[in] addr Address to write data to.
 * \param[in] len  Length in bytes to write.
 *
 * \return CMD_OK or CMD_ERROR.
 ****************************************************************************/
int CmdWrite(enum CmdChip chip, const uint8_t *buf, uint32_t addr,
        uint16_t len);

/************************************************************************//**
 * Read data from memory chip.
 *
 * \param[in] chip Chip to read data from.
 * \param[in] buf  Buffer to put the readed data into.
 * \param[in] addr Address to read data from.
 * \param[in] len  Length in bytes to read.
 *
 * \return CMD_OK or CMD_ERROR.
 ****************************************************************************/
int CmdRead(enum CmdChip chip, uint8_t *buf, uint32_t addr, uint16_t len);

/********************************************************************//**
 * Obtain programmer firmware version:
 *
 * \param[out] fwVer Firmware version: major version on the upper byte and
 *             minor version on the lower byte.
 *
 * \return 0 on success, less than 0 on error.
 ************************************************************************/
int CmdFwGet(uint16_t *fwVer);

/************************************************************************//**
 * Get mapper type from mapper number.
 *
 * \param[in] mapper Mapper number to get type from.
 *
 * \return Mapper type corresponding to mapper number, or CMD_MAPER_MAX if
 * input mapper is not supported.
 ****************************************************************************/
int CmdMapperNumToType(uint16_t mapper);

/************************************************************************//**
 * Get mapper name from mapper type
 *
 * \param[in] type Mapper type to get name from.
 *
 * \return Mapper name, or NULL if mapper is not supported.
 ****************************************************************************/
const char *CmdMapperName(int type);

/************************************************************************//**
 * Allows knowing if a mapper has variants for horizonta/vertical mirroring.
 *
 * \param[in] type Mapper type to mirroring variants.
 *
 * \return True if mapper has mirroring variants, false if it has not, =1
 * on error.
 ****************************************************************************/
int CmdMapperHasMirroringVariants(int type);

/************************************************************************//**
 * Send mapper configuration command.
 *
 * \param[in] type Mapper type to set.
 *
 * \return 0 if OK, -1 if error.
 ****************************************************************************/
int CmdMapperCfg(int type);

/************************************************************************//**
 * Program mapper bitfile to the FPGA.
 *
 * \param[in] type    Mapper type to program.
 * \param[in] vMirror Vertical mirroring when true (if applicable).
 *
 * \return 0 if OK, -1 if error.
 ****************************************************************************/
int CmdMapperProgram(int type, int vMirror);

/************************************************************************//**
 * Obtain the number of mappers supported.
 *
 * \return The number of mappers supported. Note that available mapper types
 * range from 0 to (CmdMapperNumMappers() - 1).
 ****************************************************************************/
int CmdMapperNumMappers(void);

/************************************************************************//**
 * Closes the programmer USB interface.
 ****************************************************************************/
void CmdClose(void);

#ifdef __cplusplus
}
#endif

#endif /*_CMD_H_*/

/** \} */


