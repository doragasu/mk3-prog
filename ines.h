/************************************************************************//** * \file
 *
 * \brief iNES/NES2.0 file parser.
 *
 * \defgroup ines ines
 * \{
 * \brief iNES/NES2.0 file parser.
 *
 * \author doragasu
 * \date   2018
 ****************************************************************************/
#ifndef _INES_H_
#define _INES_H_

#include <stdint.h>

/// Fill this length at least for PRG on iNES files
#define INES_FILL_PRG_KB    256

/// Available RAM sizes
enum ines2_ram_size {
    INES2_RAM_0B = 0,       ///< 0 bytes (no RAM)
    INES2_RAM_128B,         ///< 128 bytes
    INES2_RAM_256B,         ///< 256 bytes
    INES2_RAM_512B,         ///< 512 bytes
    INES2_RAM_1KB,          ///< 1 KiB
    INES2_RAM_2KB,          ///< 2 KiB
    INES2_RAM_4KB,          ///< 4 KiB
    INES2_RAM_8KB,          ///< 8 KiB
    INES2_RAM_16KB,         ///< 16 KiB
    INES2_RAM_32KB,         ///< 32 KiB
    INES2_RAM_64KB,         ///< 64 KiB
    INES2_RAM_128KB,        ///< 128 KiB
    INES2_RAM_256KB,        ///< 256 KiB
    INES2_RAM_512KB,        ///< 512 KiB
    INES2_RAM_1024KB        ///< 1 MiB
}__attribute__((packed));

/// \brief iNES header.
/// \warning This works with GCC, but other compiles can mess the bit fields
/// ordering (C standard does neither define the bit field ordering nor the
/// preprocessor constants used to determine endianness).
struct ines_header {
    char magic[4];
    uint8_t prg_16k_blocks;
    uint8_t chr_8k_blocks;
#if __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
    union {
        uint8_t flags6;
        struct {
            uint8_t mapper_low:4;
            uint8_t mirror_ctl_ignore:1;
            uint8_t trainer:1;
            uint8_t saver_mem:1;
            uint8_t mirror_vert:1;
        };
    };
    union {
        uint8_t flags7;
        struct {
            uint8_t mapper_high:4;
            uint8_t nes2_format:2;
            uint8_t playchoice10:1;
            uint8_t vs_unisystem:1;
        };
    };
    union {
        struct {                        // NES 2.0
            uint8_t submapper:4;
            uint8_t mapper_upper:4;
        } nes2_flags8;
        uint8_t ines_prg_ram_8k_blocks; // iNES
    };
    union {
        uint8_t flags9;
        struct {                    // iNES
            uint8_t reserved:7;
            uint8_t pal:1;
        } ines_flags9;
        struct {                    // NES 2.0
            uint8_t chr_8k_blocks_high:4;
            uint8_t prg_16k_blocks_high:4;
        } nes2_flags9;
    };
    union {
        uint8_t flags10;
        struct {                    // iNES
            uint8_t unused1:2;
            uint8_t bus_conflict:1;
            uint8_t prg_ram:1;
            uint8_t unused2:2;
            uint8_t tv_system:2;
        } ines_flags10;
        struct {                    // NES 2.0
            enum ines2_ram_size saver_prg_ram_len:4; 
            enum ines2_ram_size work_prg_ram_len:4; 
        } nes2_flags10;
    };
    union {
        uint8_t flags11;
        struct {
            enum ines2_ram_size saver_chr_ram_len:4;
            enum ines2_ram_size work_chr_ram_len:4;
        } nes2_flags11;
    };
    union {
        uint8_t flags12;
        struct {
            uint8_t unused:6;
            uint8_t both:1;
            uint8_t pal:1;
        } nes2_flags12;
    };
    union {
        uint8_t flags13;
        struct {
            uint8_t mode:4;
            uint8_t ppu:4;
        } nes2_flags13;
    };
    union {
        uint8_t flags14;
        struct {
            uint8_t unused:6;
            uint8_t extra_roms:2;
        } nes2_flags14;
    };
#else // LITTLE ENDIAN
    union {
        uint8_t flags6;
        struct {
            uint8_t mirror_vert:1;
            uint8_t saver_mem:1;
            uint8_t trainer:1;
            uint8_t mirror_ctl_ignore:1;
            uint8_t mapper_low:4;
        };
    };
    union {
        uint8_t flags7;
        struct {
            uint8_t vs_unisystem:1;
            uint8_t playchoice10:1;
            uint8_t nes2_format:2;
            uint8_t mapper_high:4;
        };
    };
    union {
        uint8_t flags8;
        uint8_t ines_prg_ram_8k_blocks; // iNES
        struct {                        // NES 2.0
            uint8_t mapper_upper:4;
            uint8_t submapper:4;
        } nes2_flags8;
    };
    union {
        uint8_t flags9;
        struct {                    // iNES
            uint8_t pal:1;
            uint8_t reserved:7;
        } ines_flags9;
        struct {                    // NES 2.0
            uint8_t prg_16k_blocks_high:4;
            uint8_t chr_8k_blocks_high:4;
        } nes2_flags9;
    };
    union {
        uint8_t flags10;
        struct {                    // iNES
            uint8_t tv_system:2;
            uint8_t unused2:2;
            uint8_t prg_ram:1;
            uint8_t bus_conflict:1;
            uint8_t unused1:2;
        } ines_flags10;
        struct {                    // NES 2.0
            enum ines2_ram_size work_prg_ram_len:4; 
            enum ines2_ram_size saver_prg_ram_len:4; 
        } nes2_flags10;
    };
    union {
        uint8_t flags11;
        struct {
            enum ines2_ram_size saver_chr_ram_len:4;
            enum ines2_ram_size work_chr_ram_len:4;
        } nes2_flags11;
    };
    union {
        uint8_t flags12;
        struct {
            uint8_t pal:1;
            uint8_t both:1;
            uint8_t unused:6;
        } nes2_flags12;
    };
    union {
        uint8_t flags13;
        struct {
            uint8_t ppu:4;
            uint8_t mode:4;
        } nes2_flags13;
    };
    union {
        uint8_t flags14;
        struct {
            uint8_t extra_roms:2;
            uint8_t unused:6;
        } nes2_flags14;
    };
#endif
    uint8_t zero;
};

/// Available TV systems
enum {
    INES_TV_SYSTEM_NTSC = 0,    ///< NTSC only
    INES_TV_SYSTEM_PAL = 2,     ///< PAL only
    INES_TV_SYSTEM_DUAL1 = 1,   ///< NTSC/PAL compatible
    INES_TV_SYSTEM_DUAL2 = 3    ///< NTSC/PAL compatible
};

/// Table with mapper information, to be used with expansion macros
#define INES_MAPPER_TABLE(X_MACRO)              \
    X_MACRO(  0, NROM,   "NROM")                \
    X_MACRO(  1, MMC1,   "SxROM, MMC1")         \
    X_MACRO(  2, UXROM,  "UxROM")               \
    X_MACRO(  3, CNROM,  "CNROM")               \
    X_MACRO(  4, MMC3,   "TxROM, MMC3, MMC6")   \
    X_MACRO(  5, MMC5,   "ExROM, MMC5")         \
    X_MACRO(  7, AXROM,  "AxROM")               \
    X_MACRO(  9, MMC2,   "PxROM, MMC2")         \
    X_MACRO( 10, MMC4,   "FxROM, MMC4")         \
    X_MACRO(113, MAP113, "MAPPER 113")          \
    X_MACRO(404, NFROM,  "NFROM")               \

/// Expands the mapper information table as an enumeration
#define INES_X_MACRO_AS_ENUM(num, name, str)    \
    INES_MAPPER_ ## name,

/// Supported mappers
enum ines_mapper {
    INES_MAPPER_TABLE(INES_X_MACRO_AS_ENUM)
    INES_MAPPER_MAX
};

/// Read flags structure
struct ines_read_flags {
    uint32_t fill_prg_16k_blocks;   ///< Fill at least these 16 kiB PRG blocks
    uint32_t fill_chr_8k_blocks;    ///< Fill at least these 8 kiB CHR blocks
    uint8_t verbose;                ///< Verbose output when true
};

/// Data extracted from the NES2/iNES header
struct ines_rom_data {
    struct ines_header header;      ///< Unmodified header
    uint8_t *prg_rom;               ///< PRG ROM data
    uint8_t *chr_rom;               ///< CHR ROM data
    uint32_t prg_rom_16k_blocks;    ///< Number of 16kiB PRG blocks
    uint32_t chr_rom_8k_blocks;     ///< Number of 8kiB CHR blocks
    uint16_t mapper_num;            ///< Mapper number
};

/// Return status codes
enum ines_status {
    INES_STAT_OK = 0,               ///< Success
    INES_STAT_UNSPECIFIED_ERROR,    ///< Non specified error
    INES_STAT_FILE_OPEN_ERROR,      ///< Error opening file
    INES_STAT_FILE_READ_ERROR,      ///< Error reading file
    INES_STAT_FORMAT_ERROR,         ///< iNES/NES2 format error
    INES_STAT_MEMORY_ERROR          ///< Memory error
};

#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************//**
 * \brief Parses a iNES file, allocates the buffers for the CHR and PRG ROMs
 * and reads them.
 *
 * \param[in]  file  Path to the file containing the iNES ROM.
 * \param[in]  flags Input flags for the function
 * \param[out] data  Read ROM data: CHR ROM, PRG ROM, lengths, headers and
 *                   mapper.
 *
 * \return Status code: INES_STAT_OK if the function succeeded, or the error
 *         code otherwise.
 *
 * \warning When if the function succeeds, the PRG ROM and CHR ROM buffers
 * must be deallocated externally when no longer needed, either by using
 * ines_free(), or directly using free().
 ****************************************************************************/
enum ines_status ines_load(const char *file,
        const struct ines_read_flags *flags,
        struct ines_rom_data *data);

/************************************************************************//**
 * \brief Free a ROM data structure, previously allocated with ines_load().
 *
 * \param[inout] data ROM data to deallocate.
 ****************************************************************************/
void ines_free(struct ines_rom_data *data);

/************************************************************************//**
 * \brief Obtains the mapper number from the mapper type.
 *
 * \param[in] mapper Mapper type to obtain the number.
 *
 * \return Mapper number corresponding to the mapper type.
 ****************************************************************************/
uint16_t ines_to_mapper_number(enum ines_mapper mapper);

/************************************************************************//**
 * \brief Obtains the mapper name from the mapper type.
 *
 * \param[in] mapper Mapper type to obtain the mapper name.
 *
 * \return Mapper name corresponding to the mapper type.
 ****************************************************************************/
const char *ines_to_mapper_str(enum ines_mapper mapper);

#ifdef __cplusplus
}
#endif

#endif /*_INES_H_*/

/** \} */

