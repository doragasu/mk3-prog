var indexSectionsWithContent =
{
  0: "_abcdefhilmnprstuvw",
  1: "acfim",
  2: "acfilmpuv",
  3: "abcdefilmnprstvw",
  4: "abcdefhilmnprstv",
  5: "cis",
  6: "ci",
  7: "_acilps",
  8: "acfilmpuv",
  9: "mt"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "defines",
  8: "groups",
  9: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Enumerator",
  7: "Macros",
  8: "Modules",
  9: "Pages"
};

