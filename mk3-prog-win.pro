######################################################################
# mk3-prog qmake project file for 32-bit windows builds.
#
# doragasu, 2018
######################################################################

TEMPLATE = app
TARGET = mk3-prog
INCLUDEPATH += .
PREFIX = i686-w64-mingw32-
# mingw xml2-config does not create a i686-w64-mingw32-xml2-config symlink
# so we need to specify the full path
XML2BIN = /usr/i686-w64-mingw32/bin

QT += widgets

DEFINES += QT_DEPRECATED_WARNINGS

# Uncomment following three lines for Windows static builds
CONFIG+=static
CONFIG+=no_smart_library_merge
QTPLUGIN+=qwindows

GLIBINC = $$system($${PREFIX}pkg-config --cflags glib-2.0)
GLIBLIB = $$system($${PREFIX}pkg-config --libs glib-2.0)
XML2INC = $$system($$XML2BIN/xml2-config --cflags)
XML2LIB = $$system($$XML2BIN/xml2-config --libs)

# Extra headers
QMAKE_CXXFLAGS += $$GLIBINC $$XML2INC
QMAKE_CFLAGS += $$GLIBINC $$XML2INC
# Extra libraries
#LIBS += $$GLIBLIB -lusb-1.0 -lutil
LIBS += $$GLIBLIB $$XML2LIB -lusb-1.0

DEFINES += QT

# Input files
HEADERS = avrflash.h  cmd.h  flashdlg.h  flash_man.h  ines.h  latticeflash.h  progbar.h  pspawn.h  util.h version.h
SOURCES += avrflash.c  cmd.c  flashdlg.cpp  flash_man.cpp  ines.c  latticeflash.c  main.cpp  progbar.c  pspawn.c util.c
