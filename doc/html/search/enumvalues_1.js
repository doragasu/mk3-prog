var searchData=
[
  ['ines_5fstat_5ffile_5fopen_5ferror',['INES_STAT_FILE_OPEN_ERROR',['../group__ines.html#ggaed85a1c07ec94cfe1ae011093cdf091ea89172ca10b716b2b2b6fba8cd6e81b4b',1,'ines.h']]],
  ['ines_5fstat_5ffile_5fread_5ferror',['INES_STAT_FILE_READ_ERROR',['../group__ines.html#ggaed85a1c07ec94cfe1ae011093cdf091eace291ae1ba9dd2038f05fb5f692b3e02',1,'ines.h']]],
  ['ines_5fstat_5fformat_5ferror',['INES_STAT_FORMAT_ERROR',['../group__ines.html#ggaed85a1c07ec94cfe1ae011093cdf091ea03c988a5bd754ad87cf87d5165f32264',1,'ines.h']]],
  ['ines_5fstat_5fmemory_5ferror',['INES_STAT_MEMORY_ERROR',['../group__ines.html#ggaed85a1c07ec94cfe1ae011093cdf091eac24df0e6d6b91319e780ac2bb40758ee',1,'ines.h']]],
  ['ines_5fstat_5fok',['INES_STAT_OK',['../group__ines.html#ggaed85a1c07ec94cfe1ae011093cdf091ea2ad3566c98b1f578658fd2a2f1a99d7f',1,'ines.h']]],
  ['ines_5fstat_5funspecified_5ferror',['INES_STAT_UNSPECIFIED_ERROR',['../group__ines.html#ggaed85a1c07ec94cfe1ae011093cdf091ea07111fab078a2c15241c77d0059b8e19',1,'ines.h']]],
  ['ines_5ftv_5fsystem_5fdual1',['INES_TV_SYSTEM_DUAL1',['../group__ines.html#gga99fb83031ce9923c84392b4e92f956b5aa415a3b78690d78dc869c03ed9158131',1,'ines.h']]],
  ['ines_5ftv_5fsystem_5fdual2',['INES_TV_SYSTEM_DUAL2',['../group__ines.html#gga99fb83031ce9923c84392b4e92f956b5ab2f68be6920fe99c3bed6d3f7624f96a',1,'ines.h']]],
  ['ines_5ftv_5fsystem_5fntsc',['INES_TV_SYSTEM_NTSC',['../group__ines.html#gga99fb83031ce9923c84392b4e92f956b5ac17b0049acd446c170e634b791a68b68',1,'ines.h']]],
  ['ines_5ftv_5fsystem_5fpal',['INES_TV_SYSTEM_PAL',['../group__ines.html#gga99fb83031ce9923c84392b4e92f956b5aa01f16e7e504d8e512a678332a4690a7',1,'ines.h']]],
  ['ines_5ftype_5farchaic_5fines',['INES_TYPE_ARCHAIC_INES',['../ines_8c.html#add04930cf988567cd4637acad7829c39a7e214bacbb495b5a10029a5d420bb6c1',1,'ines.c']]],
  ['ines_5ftype_5fines',['INES_TYPE_INES',['../ines_8c.html#add04930cf988567cd4637acad7829c39a54c46fe8ec1f86c0aecf90448ddc6f39',1,'ines.c']]],
  ['ines_5ftype_5fnes2',['INES_TYPE_NES2',['../ines_8c.html#add04930cf988567cd4637acad7829c39af93557e749a863eb6c81a87e9591d97d',1,'ines.c']]]
];
